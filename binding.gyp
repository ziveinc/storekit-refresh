{
  "targets": [
    {
      "target_name": "storekitrefresh",
      # "include_dirs": [
      #   "<!(node -e \"require('nan')\")"
      # ],
      "conditions": [
        ['OS=="mac"', {
          "sources": ["lib/storekit.mm", "lib/storekitmac.mm", "lib/storekitrefresh.mm"],
          "xcode_settings": {
              "OTHER_CPLUSPLUSFLAGS": ["-std=c++17", "-stdlib=libc++", "-mmacosx-version-min=10.11"],
              "OTHER_LDFLAGS": ["-framework Cocoa -framework CoreFoundation -framework StoreKit"]
          }
        }]
      ]
    }
  ]
}
