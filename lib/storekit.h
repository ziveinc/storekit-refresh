// myobject.h
#ifndef STOREKITREFRESH_H
#define STOREKITREFRESH_H
#include <node.h>
#include <node_object_wrap.h>
// #include <nan.h>
#include "storekitmac.h"

namespace storekitrefresh {

class StoreKit : public node::ObjectWrap {
 public:
  static void Init(v8::Local<v8::Object> exports);

 private:
  explicit StoreKit();
  ~StoreKit();

  static void New(const v8::FunctionCallbackInfo<v8::Value>& args);
//   static void PlusOne(const v8::FunctionCallbackInfo<v8::Value>& args);

  StoreKitMac* storeKitMac;
  static void RefreshReceipt(const v8::FunctionCallbackInfo<v8::Value>& args);

  // void Callback();
  // Nan::Callback* callback;
};

}  // namespace demo

#endif