#ifdef __OBJC__
#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#endif
#include <functional>

@interface StoreKitMac : NSObject<SKRequestDelegate> {
    // std::function<void()> callback;
}
@property (retain) SKRequest* ourRequest;
// - (void) setCallback:(std::function<void()>)cb;
- (void) makeRequest;
- (void)requestDidFinish:(SKRequest *)request;
- (void)request:(SKRequest *)request didFailWithError:(NSError *)error;

@end 