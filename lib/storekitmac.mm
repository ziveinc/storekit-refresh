#import "storekitmac.h"

@implementation StoreKitMac

// - (void) setCallback:(std::function<void()>)cb {
//     callback = cb;
//  }

- (void) makeRequest {
    _ourRequest = [[SKReceiptRefreshRequest alloc] init];
    _ourRequest.delegate = self;
    [_ourRequest start];
}

- (void)requestDidFinish:(SKRequest *)request {
    // dispatch_async(dispatch_get_main_queue(), ^{
        // callback();
    // });
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    // dispatch_async(dispatch_get_main_queue(), ^{
        // callback();
    // });
}

@end