#include <node.h>
#include <v8.h>
#include "storekit.h"

namespace storekitrefresh {
    using namespace v8;

    void InitAll(Local<Object> exports) {
        StoreKit::Init(exports);
    }

    NODE_MODULE(NODE_GYP_MODULE_NAME, InitAll)
};