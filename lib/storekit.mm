#include "storekit.h"

namespace storekitrefresh {

using v8::Context;
using v8::Function;
using v8::FunctionCallbackInfo;
using v8::FunctionTemplate;
using v8::Isolate;
using v8::Local;
using v8::Persistent;
using v8::Number;
using v8::Object;
using v8::ObjectTemplate;
using v8::String;
using v8::Value;

StoreKit::StoreKit() {
  storeKitMac = [[StoreKitMac alloc] init];
  // [storeKitMac setCallback:std::bind(&StoreKit::Callback, this)];
}

StoreKit::~StoreKit() {
}

void StoreKit::Init(Local<Object> exports) {
  Isolate* isolate = exports->GetIsolate();
  Local<Context> context = isolate->GetCurrentContext();

  Local<ObjectTemplate> addon_data_tpl = ObjectTemplate::New(isolate);
  addon_data_tpl->SetInternalFieldCount(1);  // 1 field for the MyObject::New()
  Local<Object> addon_data =
      addon_data_tpl->NewInstance(context).ToLocalChecked();

  // Prepare constructor template
  Local<FunctionTemplate> tpl = FunctionTemplate::New(isolate, New, addon_data);
  tpl->SetClassName(String::NewFromUtf8(isolate, "StoreKit", v8::NewStringType::kNormal).ToLocalChecked());
  tpl->InstanceTemplate()->SetInternalFieldCount(1);

  // Prototype
  // NODE_SET_PROTOTYPE_METHOD(tpl, "plusOne", PlusOne);
  NODE_SET_PROTOTYPE_METHOD(tpl, "RefreshReceipt", StoreKit::RefreshReceipt);

  Local<Function> constructor = tpl->GetFunction(context).ToLocalChecked();
  addon_data->SetInternalField(0, constructor);
  exports->Set(context, String::NewFromUtf8(
      isolate, "StoreKit", v8::NewStringType::kNormal).ToLocalChecked(),
      constructor).FromJust();
}

void StoreKit::New(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = args.GetIsolate();
  Local<Context> context = isolate->GetCurrentContext();

  if (args.IsConstructCall()) {
    // Invoked as constructor: `new MyObject(...)`
    // double value = args[0]->IsUndefined() ?
    //     0 : args[0]->NumberValue(context).FromMaybe(0);
    StoreKit* obj = new StoreKit();
    obj->Wrap(args.This());
    args.GetReturnValue().Set(args.This());
  } else {
    // Invoked as plain function `MyObject(...)`, turn into construct call.
    // corst int argc = 1;
    // Local<Value> argv[argc] = { args[0] };
    Local<Function> cons =
        args.Data().As<Object>()->GetInternalField(0).As<Function>();
    Local<Object> result =
        cons->NewInstance(context, 0, nil).ToLocalChecked();
    args.GetReturnValue().Set(result);
  }
}

void StoreKit::RefreshReceipt(const FunctionCallbackInfo<Value>& args) {
  //Isolate* isolate = args.GetIsolate();
  //Local<Context> context = isolate->GetCurrentContext();

  // check if our first arg is a function
  // if (!args[0]->IsFunction()) {
    // isolate->ThrowException(String::NewFromUtf8(isolate, "RefreshReceipt requires a callback as the first argument", v8::NewStringType::kNormal).ToLocalChecked());
    // return;
  // }

  StoreKit* obj = ObjectWrap::Unwrap<StoreKit>(args.Holder());

  // v8::Local<v8::Function> func = v8::Local<v8::Function>::Cast(args[0]);

  // obj->iso = isolate;
  // obj->callback.Reset(isolate, func);
  // obj->callback = new Nan::Callback(args[0].As<v8::Function>());

  // obj->callback = v8::Persistent<v8::Function>::New(isolate, args[0]);

  [obj->storeKitMac makeRequest];
}

// void StoreKit::Callback() {
//   // Isolate* isolate = Isolate::GetCurrent();
//   // Local<Context> context = isolate->GetCurrentContext();
//   // Local<Function> func = Local<Function>::New(isolate, callback);
//   // Local<Context> context = iso->GetCurrentContext();
//   // Local<Function> func = Local<Function>::New(iso, callback);
//   // const unsigned argc = 1;
//   // Local<Value> argv[argc] = {
//       // String::NewFromUtf8(isolate,
//                           // "hello world",
//                           // v8::NewStringType::kNormal).ToLocalChecked() };
//   // func->Call(context, Null(isolate), argc, argv);
//   // callback.Call(Context::GetCurrent()->Global(), 0, argv);
//   Nan::Call(*callback, Nan::GetCurrentContext()->Global(), 0, NULL);
// }
}  // namespace demo
